<?php
class Calculator {
    /**
     * @var array Possible operators
     */
    private $_operators = ['+', '-', '*', '/', '^', 'mod'];
     /**
     * @var array Defined functions.
     */
    private $_functions = [];
     /**
     * @var string Current arithmetic expression.
     */
    private $_expression;

    /**
     * @param  string $operator A valid operator.
     * @return int
     * @throws \InvalidArgumentException
     */
    private function getPrecedence($operator) {
        if(!in_array($operator, $this->_operators)) {
            throw new \InvalidArgumentException("Cannot check precedence of $operator operator");
        }
        if($operator === '^') {
            return 6;
        }
        else if($operator === '*' || $operator === '/') {
            return 4;
        }
        else if($operator === 'mod') {
            return 2;
        }
        return 1;
    }
    /**
     * @param  string $operator A valid operator.
     * @param  int $a First value.
     * @param  int $b Second value.
     * @return int Result.
     * @throws \InvalidArgumentException
     */
    private function executeOperator($operator, $a, $b) {
        if($operator === '+') {
            return $a + $b;
        }
        else if($operator === '-') {
            return $b - $a;
        }
        else if($operator === 'mod') {
            return $b % $a;
        }
        else if($operator === '*') {
            return $a * $b;
        }
        else if($operator === '/') {
            if($a === 0) {
                throw new \InvalidArgumentException('Division by zero');
            }
            return $b / $a;
        }
        else if($operator === '^') {
            return pow($b, $a);
        }
        throw new \InvalidArgumentException('Unknown operator');
    }

     /**
     * Calculates in order.
     *
     * @param  $queue (Splqueue??)
     * @return int Result of the calculation.
     * @throws \InvalidArgumentException
     * @todo check for invalid expression inside if & else statements
     */
    public function calculateFromPostfix($queue) {
        $stack = new Stack();
        while($queue->count() > 0) {
            $currentToken = $queue->dequeue();
            if(is_numeric($currentToken)) {
                $stack->push($currentToken);
            }
            else {
                if(in_array($currentToken, $this->_operators)) {
                    // Check for invalid expression goes here...
                    $stack->push($this->executeOperator($currentToken, $stack->pop(), $stack->pop()));
                }
            }
        }
        if($stack->count() === 1) {
            return $stack->pop();
        }
        throw new \InvalidArgumentException('Invalid expression');
    }

     /**
     * Calculates the current arithmetic expression.
     * @return int Result of the calculation.
     */
    public function calculate() {
        $tokens = $this->_scanner->getTokens($this->_expression); //Get all tokens
        $postfix    = $this->getPostfixNotation($tokens); //Order everything into post-fix notation based on precedence
        $result = $this->calculateFromPostfix($postfix); //Not quite finished
        return $result;
    }
}
?>