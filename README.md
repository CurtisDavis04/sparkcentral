##PHP Calculator for arithmetic expressions##

###PHP calculator which evaluates different arithmetic expressions:###
```
Examples here...
```

###Intended Basic usage###

```
$calculator = new Calculator();
$calculator->setExpression('1+2*3/4');

echo $calculator->calculate(); // 2.5
```

###TODO List###

```
* Add ability for user-defined functions to be added (square root, log, etc...)
* Complete project for actual use
* Increase performance
```